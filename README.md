<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel VueJS with AdminLTE3 Integration
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravue-adminlte3.git`
2. Go to folder: `cd laravue-adminlte3`
3. Run `composer install`
4. Run `php artisan serve`
5. Open your favorite browser: http://localhost:8000/

### Screen shot

AdminLTE3 Home Page

![AdminLTE3 Home Page](img/index.png "AdminLTE3 Home Page") 
